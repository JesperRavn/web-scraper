#!/usr/bin/env python
# -*- coding: utf-8 -*-
import urlparse
import re
import mechanize
from StringIO import StringIO
from urllib2 import urlopen, Request
import slate


class spider():
    def __init__(self, url):
        self.scraped = []
        self.url = url

    def _uniqueappender(self, items):
        for item in items:
            if item not in self.scraped:
                self.scraped.append(item)

    def sniffer(self, text):
        emailregex = ("([a-z\!\#\$\%\&\'\*\+\-\/"
                      "\=\?\^\_\`\{\|\}\~\.]+@[a-zA-Z0-9]"
                      "+[a-zA-Z0-9\-]*[a-zA-Z0-9]+\.[a-z]{2,3})")
        return re.findall(emailregex, text)

    def _ispdf(self, link):
        pdfregex = ".*\.pdf$"
        return bool(re.match(pdfregex, link))

    def _pdfreader(self, url):
        open = urlopen(Request(url)).read()
        memoryFile = StringIO(open)
        return slate.PDF(memoryFile).text()

    def crawl(self, wild=False, filepath="", consolelog=False,
              readpdf=True, depth=0):
        stack = [self.url]
        history = [self.url]
        br = mechanize.Browser()
        while stack:
            if self._ispdf(stack[0]) and readpdf:
                try:
                    if consolelog:
                        print "scraping: ", stack[0]
                    self._uniqueappender(
                        self.sniffer(self._pdfreader(stack[0])))
                    if consolelog:
                        print "New stack length: ", len(stack)
                        print "Items found: ", len(self.scraped)
                except Exception as e:
                    if consolelog:
                        print e
                        print "couldn't read: ", stack[0], "\n"
                finally:
                    stack.pop(0)
            else:
                try:
                    page = br.open(stack[0]).read()
                    if consolelog:
                        print "scraping: ", stack[0]
                    self._uniqueappender(self.sniffer(page))
                    if consolelog:
                        print "New stack length: ", len(stack)
                        print "Items found: ", len(self.scraped), "\n"
                    for link in br.links():
                        newurl = urlparse.urljoin(link.base_url, link.url)
                        if newurl not in history and self.url in newurl and wild == False:
                            stack.append(newurl)
                            history.append(newurl)
                        elif newurl not in history and wild is True:
                            stack.append(newurl)
                            history.append(newurl)
                except Exception as e:
                    if consolelog:
                        print e
                        print "couldn't read: ", stack[0], "\n"
                finally:
                    stack.pop(0)
        if filepath is not "":
            with open(filepath, 'w') as f:
                for item in self.scraped:
                    f.writelines(item + '\n')

