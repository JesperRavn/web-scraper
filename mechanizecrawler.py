import re
import slate
from urllib2 import Request
from StringIO import StringIO
from urllib2 import urlopen, Request


url = 'http://www.kirkegaardskultur.dk/fileadmin/group/532/Aarsskrift/294840_Aarsskrift_2009_Samlet.pdf'
# req = Request(url)
# opens = urlopen(req).read()
# memoryFile = StringIO(open)


with open(url) as f:
    doc = slate.PDF(f)

emailregex = ("([a-z\!\#\$\%\&\'\*\+\-\/"
              "\=\?\^\_\`\{\|\}\~\.]+@[a-zA-Z0-9]"
              "+[a-zA-Z0-9\-]*[a-zA-Z0-9]+\.[a-z]{2,3})")

print re.findall(emailregex, doc.text())
