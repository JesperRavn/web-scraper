from StringIO import StringIO
from urllib2 import urlopen, Request
import slate
import re

def uniqueappender(self, items):
        for item in items:
            if item not in self.scraped:
                self.scraped.append(item)

def sniffer(text):
    emailregex = ("([a-z\!\#\$\%\&\'\*\+\-\/"
                  "\=\?\^\_\`\{\|\}\~\.]+@[a-zA-Z0-9]"
                  "+[a-zA-Z0-9\-]*[a-zA-Z0-9]+\.[a-z]{2,3})")
    return re.findall(emailregex, text)



def pdfreader(url):
    open = urlopen(Request(url)).read()
    memoryFile = StringIO(open)
    return slate.PDF(memoryFile).text()

text = pdfreader('http://www.kirkegaardskultur.dk/fileadmin/group/532/Aarsskrift/294840_Aarsskrift_2009_Samlet.pdf')
print sniffer(text)
